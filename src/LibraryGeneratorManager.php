<?php

namespace Drupal\library_generator;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Drupal\Core\Plugin\Factory\ContainerFactory;

/**
 * Defines a plugin manager to deal with library_generator plugins.
 *
 * Modules can define library_generator plugins in a
 * MODULE_NAME.library_generator.yml file contained in the module's base
 * directory. Each library_generator has the following structure:
 *
 * @code
 *   MODULE_NAME.libraries:
 *     css:
 *       base:
 *         path/to/base/dir: {}
 *       layout:
 *         path/to/layout/dir: {}
 *       component:
 *         path/to/component/dir: {}
 *       state:
 *         path/to/state/dir: {}
 *       theme:
 *         path/to/theme/dir: {}
 *     js:
 *       path/to/js/dir: { weight: -5 }
 * @endcode
 *
 * @see \Drupal\library_generator\LibraryGeneratorDefault
 * @see \Drupal\library_generator\LibraryGeneratorInterface
 */
final class LibraryGeneratorManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  protected $defaults = [
    'id' => '',
    'css' => [],
    'js' => [],
    'class' => LibraryGenerator::class,
  ];

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Plugin definitions by provider.
   *
   * @var array
   */
  protected $generatorsByProvider;

  /**
   * The plugin instances.
   *
   * @var array
   */
  protected $instances;

  /**
   * Constructs a new LibraryGeneratorManager instance.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   */
  public function __construct(CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler) {
    $this->factory = new ContainerFactory($this);
    $this->moduleHandler = $module_handler;
    $this->themeHandler = $theme_handler;
    $this->alterInfo('library_generator_info');
    $this->setCacheBackend($cache_backend, 'library_generator_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery(): YamlDiscovery {
    if (!isset($this->discovery)) {
      $this->discovery = new YamlDiscovery('library_generator', $this->moduleHandler->getModuleDirectories() + $this->themeHandler->getThemeDirectories());
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  protected function providerExists($provider) {
    return $this->moduleHandler->moduleExists($provider) || $this->themeHandler->themeExists($provider);
  }

  /**
   * Gets library_generator plugins for the specified provider.
   *
   * @param string $provider
   *   The plugin provider.
   *
   * @return \Drupal\library_generator\LibraryGenerator[]
   *   Array of library_generator plugins keyed by machine name.
   */
  public function getGeneratorsByProvider(string $provider): array {
    if (!isset($this->generatorsByProvider[$provider])) {
      if ($cache = $this->cacheBackend->get($this->cacheKey . ':' . $provider)) {
        $this->generatorsByProvider[$provider] = $cache->data;
      }
      else {
        $generators = [];
        foreach ($this->getDefinitions() as $plugin_id => $plugin_definition) {
          if ($plugin_definition['provider'] == $provider) {
            $generators[$plugin_id] = $plugin_definition;
          }
        }
        $this->cacheBackend->set($this->cacheKey . ':' . $provider, $generators, Cache::PERMANENT, ['generators']);
        $this->generatorsByProvider[$provider] = $generators;
      }
    }

    $instances = [];
    foreach ($this->generatorsByProvider[$provider] as $plugin_id => $definition) {
      if (!isset($this->instances[$plugin_id])) {
        $this->instances[$plugin_id] = $this->createInstance($plugin_id);
      }
      $instances[$plugin_id] = $this->instances[$plugin_id];
    }
    return $instances;
  }

}
