<?php

namespace Drupal\library_generator;

/**
 * Interface for library_generator plugins.
 */
interface LibraryGeneratorInterface {

  /**
   * Returns the css array.
   */
  public function getCss(): array;

  /**
   * Returns the js array.
   */
  public function getJs(): array;

}
