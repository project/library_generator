<?php

namespace Drupal\library_generator;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\File\FileSystemInterface;

/**
 * Discovers asset libraries based on library_generator plugins.
 */
class LibraryGeneratorDiscovery {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cacheBackend;

  /**
   * The library generator plugin manager.
   *
   * @var \Drupal\library_generator\LibraryGeneratorManager
   */
  protected LibraryGeneratorManager $libraryGeneratorManager;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $moduleExtensionList;

  /**
   * The theme extension list.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected ThemeExtensionList $themeExtensionList;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The library definitions.
   *
   * @var array
   */
  protected $libraryDefinitions = [];

  /**
   * The cache key.
   *
   * @var string
   */
  protected $cacheKey = 'library_generator_discovery';

  /**
   * Constructs a LibraryGeneratorDiscovery object.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\library_generator\LibraryGeneratorManager $library_generator_manager
   *   The library generator plugin manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list.
   * @param \Drupal\Core\Extension\ThemeExtensionList $theme_extension_list
   *   The theme extension list.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(CacheBackendInterface $cache_backend, LibraryGeneratorManager $library_generator_manager, ModuleExtensionList $module_extension_list, ThemeExtensionList $theme_extension_list, FileSystemInterface $file_system) {
    $this->cacheBackend = $cache_backend;
    $this->libraryGeneratorManager = $library_generator_manager;
    $this->moduleExtensionList = $module_extension_list;
    $this->themeExtensionList = $theme_extension_list;
    $this->fileSystem = $file_system;
  }

  /**
   * Get asset library definitions for the specified extension.
   *
   * @param string $extension
   *   The extension name.
   *
   * @return array
   *   An array of library definitions.
   */
  public function getLibrariesByExtension($extension) {
    if (!isset($this->libraryDefinitions[$extension])) {
      if ($cache = $this->cacheBackend->get($this->cacheKey . ':' . $extension)) {
        $this->libraryDefinitions[$extension] = $cache->data;
      }
      else {
        $libraries = $this->buildByExtension($extension);
        $this->cacheBackend->set($this->cacheKey . ':' . $extension, $libraries, Cache::PERMANENT, ['libraries']);
        $this->libraryDefinitions[$extension] = $libraries;
      }
    }
    return $this->libraryDefinitions[$extension];
  }

  /**
   * Build asset library definitions for the specified extension.
   *
   * @param string $extension
   *   The extension name.
   *
   * @return array
   *   An array of library definitions.
   */
  protected function buildByExtension($extension) {
    $generators = $this->libraryGeneratorManager->getGeneratorsByProvider($extension);

    if (empty($generators)) {
      return [];
    }

    $libraries = [];
    $extension_path = $this->getExtensionPath($extension);
    foreach ($generators as $generator) {
      foreach ($generator->getCss() as $level => $directories) {
        foreach ($directories as $directory => $params) {
          $scan_path = "$extension_path/$directory";
          if (!is_dir($scan_path)) {
            continue;
          }
          $files = $this->fileSystem->scanDirectory($scan_path, '/\.css$/');
          foreach ($files as $file) {
            $library_name = $file->name;
            $asset_path = substr($file->uri, strlen($extension_path) + 1);
            $libraries[$library_name]['css'][$level][$asset_path] = $params;
            $hash = hash_file('crc32b', $file->uri);
            if (!isset($libraries[$library_name]['version'])) {
              $libraries[$library_name]['version'] = $hash;
            }
            else {
              $libraries[$library_name]['version'] = hash('crc32b', $libraries[$library_name]['version'] . $hash);
            }
          }
        }
      }
      foreach ($generator->getJs() as $directory => $params) {
        $scan_path = "$extension_path/$directory";
        if (!is_dir($scan_path)) {
          continue;
        }
        $files = $this->fileSystem->scanDirectory($scan_path, '/\.js$/');
        foreach ($files as $file) {
          $library_name = $file->name;
          $asset_path = substr($file->uri, strlen($extension_path) + 1);
          $libraries[$library_name]['js'][$asset_path] = $params;
          $hash = hash_file('crc32b', $file->uri);
          if (!isset($libraries[$library_name]['version'])) {
            $libraries[$library_name]['version'] = $hash;
          }
          else {
            $libraries[$library_name]['version'] = hash('crc32b', $libraries[$library_name]['version'] . $hash);
          }
        }
      }
    }
    return $libraries;
  }

  /**
   * Get the path of the specified extension.
   *
   * @param string $extension
   *   The extension name.
   *
   * @return string
   *   The extension path.
   */
  protected function getExtensionPath(string $extension): string {
    if ($this->moduleExtensionList->exists($extension)) {
      return $this->moduleExtensionList->getPath($extension);
    }
    if ($this->themeExtensionList->exists($extension)) {
      return $this->themeExtensionList->getPath($extension);
    }
    return '';
  }

}
