<?php

namespace Drupal\library_generator;

use Drupal\Core\Plugin\PluginBase;

/**
 * Default class used for library_generator plugins.
 */
final class LibraryGenerator extends PluginBase implements LibraryGeneratorInterface {

  /**
   * {@inheritdoc}
   */
  public function getCss(): array {
    // The title from YAML file discovery may be a TranslatableMarkup object.
    return $this->pluginDefinition['css'];
  }

  /**
   * {@inheritdoc}
   */
  public function getJs(): array {
    // The title from YAML file discovery may be a TranslatableMarkup object.
    return $this->pluginDefinition['js'];
  }

}
