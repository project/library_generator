## INTRODUCTION

The Library Generator module generates asset libraries for CSS and JS files in
modules and themes.

Use this module if you want a separate asset library for each CSS and JS file
without having to add them manually to your `*.libraries.yml`.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

Suppose you have the following file structure:

```
MODULE_NAME
  dist
    css
      0
        lib1.css
        lib2.css
      1
        lib3.css
    js
      0
        lib1.js
        lib2.js
      1
        lib3.js
```

With the following configuration in `MODULE_NAME.library_generator.yml`:

```yaml
MODULE_NAME.libraries:
  css:
    base:
      dist/css/0: {}
    layout:
      dist/css/1: {}
    component:
      dist/css/2: {}
    state:
      dist/css/3: {}
    theme:
      dist/css/4: {}
  js:
    dist/js/0: { weight: -5 }
    dist/js/1: { weight: -4 }
    dist/js/2: { weight: -3 }
    dist/js/3: { weight: -2 }
    dist/js/4: { weight: -1 }
```

The module will scan the configured directories and generate the following
libraries:

```yaml
lib1:
  css:
    base:
      dist/css/0/lib1.css: {}
  js:
    dist/js/0/lib1.js: { weight: -5 }
lib2:
  css:
    base:
      dist/css/0/lib2.css: {}
  js:
    dist/js/0/lib2.js: { weight: -5 }
lib3:
  css:
    layout:
      dist/css/1/lib3.css: {}
  js:
    dist/js/1/lib3.js: { weight: -4 }
```

## MAINTAINERS

Current maintainers:

- Parkle Lee (phparkle) - https://www.drupal.org/u/phparkle
